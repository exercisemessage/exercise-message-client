/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package exercise.message.client;

import com.exercise.message.entities.Resultado;
import com.exercise.message.entities.input.ListarMensajeInput;
import exercise.message.entitie.Input;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import exercise.message.entitie.Credenciales;
import exercise.message.entitie.DeleteUsuarioInput;
import exercise.message.entitie.MensajeOutput;
import exercise.message.entitie.NewUsuarioInput;
import exercise.message.entitie.SendMensajeInput;
import exercise.message.entitie.UpdateUsuarioInput;
import exercise.message.entitie.Usuario;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;

/**
 *
 * @author SISPEDRV
 */
public class ExerciseMessageClient {

    /**
     * @param args the command line arguments
     */
   
   public static final String ip_expresion_regular="(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5]})";
   public static final String port_expresion_regular="[0-9]*";
    
   public String validarUsuario(String us){
       if(us.isEmpty()){
           return "El usuario no puede ser vacio";
       }
             
       return "";
   }
   
   public String validarUpdate(String antigo, String nuevo){
       if(antigo.isEmpty()){
           return "El usuario antiguo no puede ser vacio";
       }
       if(nuevo.isEmpty()){
           return "El usuario nuevo no puede ser vacio";
       }
             
       return "";
   }
   
    public String validarMensaje(String de, String para,String mensaje){
       if(de.isEmpty()){
           return "El campo de no puede ser vacio";
       }
       if(para.isEmpty()){
           return "El campo para no puede ser vacio";
       }
       if(mensaje.isEmpty()){
           return "El campo mensaje no puede ser vacio";
       }
              
       return "";
   }
    
    public String validarParametros(String us, String pw,String ip, String port){
       if(us.isEmpty()){
           return "El usuario no puede ser vacio";
       }
       if(pw.isEmpty()){
           return "El password no puede ser vacio";
       }
       if(ip.isEmpty()){
           return "La ip no puede ser vacio";
       }
        if(!ip.matches(ip_expresion_regular)){
            return "ip invalida";
        }
         if(port.isEmpty()){
            return "El puerto no puede ser vacio";
        }
        if(!port.matches(port_expresion_regular)){
            return "port invalida";
        }
       
       
       return "";
   }
  
    public String consumirAddUsuario(Credenciales cre, String us, String miip,String url){
        try {
            NewUsuarioInput nui=new NewUsuarioInput();
            nui.setNombre(us);

            String uSgson= new Gson().toJson(nui);

            Input input = new Input();
            input.setCredenciales(cre.Encriptar(miip));
            System.out.println("cred: "+input.getCredenciales());
            input.setObject(uSgson);

            uSgson= new Gson().toJson(input);
            
            return consumirPost(url, uSgson);
        } catch (Exception e) {
            e.printStackTrace();
            return "Error al consumir";
        } 
    }
    
    public String consumirUpdateUsuario(Credenciales cre, String antiguo,String nuevo, String miip,String url){
        try {
            UpdateUsuarioInput uui= new UpdateUsuarioInput();
            uui.setNewNombre(nuevo);
            uui.setOldNombre(antiguo);

            String uSgson= new Gson().toJson(uui);

            Input input = new Input();
            input.setCredenciales(cre.Encriptar(miip));
            System.out.println("cred: "+input.getCredenciales());
            input.setObject(uSgson);

            uSgson= new Gson().toJson(input);
            
            return consumirPost(url, uSgson);
        
        } catch (Exception e) {
            e.printStackTrace();
            return "Error al consumir";
        } 
    }
    
    public String consumirdeleteUsuario(Credenciales cre, String us, String miip,String url){
        try {
            DeleteUsuarioInput dui= new DeleteUsuarioInput();
            dui.setNombre(us);

            String uSgson= new Gson().toJson(dui);

            Input input = new Input();
            input.setCredenciales(cre.Encriptar(miip));
            System.out.println("cred: "+input.getCredenciales());
            input.setObject(uSgson);

            uSgson= new Gson().toJson(input);
            
            return consumirPost(url, uSgson);
        
        } catch (Exception e) {
            e.printStackTrace();
            return "Error al consumir";
        } 
    }
    
    public String consumirSendMessage(Credenciales cre, String de,String para,String mensaje, String miip,String url){
        try {
            SendMensajeInput smi= new SendMensajeInput();
            smi.setMensaje(mensaje);
            smi.setNombreFrom(de);
            smi.setNombreTo(para);

            String uSgson= new Gson().toJson(smi);

            Input input = new Input();
            input.setCredenciales(cre.Encriptar(miip));
            System.out.println("cred: "+input.getCredenciales());
            input.setObject(uSgson);

            uSgson= new Gson().toJson(input);
            
            return consumirPost(url, uSgson);
        
        } catch (Exception e) {
            e.printStackTrace();
            return "Error al consumir";
        } 
    }
    
    public String consumirGetMessage(Credenciales cre, String us, String miip,String url){
        try {
//            ListarMensajeInput lmi= new ListarMensajeInput();
//            lmi.setNombre(us);
//            lmi.setCredenciales(cre.Encriptar(miip));
//            System.out.println(cre.Encriptar(miip));    
//            String uSgson= new Gson().toJson(lmi);
            String res=consumirGet(url,us, cre.Encriptar(miip));
            Resultado resultado=new Gson().fromJson(res, Resultado.class);
            if(resultado.getCodigo()!=0){
                return resultado.getMensaje();
            }
            Type listType = new TypeToken<List<MensajeOutput>>() {}.getType();
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("yyyy-MM-dd");
            Gson gson = gsonBuilder.create();
            List<MensajeOutput> lista=gson.fromJson(resultado.getObjetoJson(),listType );
            String uSgson =new Gson().toJson(lista);
            return uSgson;
        
        } catch (Exception e) {
            e.printStackTrace();
            return "Error al consumir";
        } 
    }
   
    
    public String consumirPost(String surl, String textoPost){
         try {
            // URL url= new URL("http://172.31.89.249:8086/PruebaRest/rest/Servicios/Save");
             URL url= new URL(surl);
             HttpURLConnection conn = (HttpURLConnection) url.openConnection();
             conn.setDoOutput(true);
             conn.setRequestMethod("POST");
             conn.setRequestProperty("Content-Type", "application/json");
             
             OutputStream os = conn.getOutputStream();
             os.write(textoPost.getBytes());
             os.flush();
             
             if(conn.getResponseCode() != HttpURLConnection.HTTP_OK){
                 throw  new RuntimeException("FAILED : HTTP error code: "+ conn.getResponseCode());
             }
             
             BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
             String output;
             String res="";
             System.out.println("OUTPUT from server ....");
             while((output = br.readLine())!=null){
                 System.out.println(output);
                 res=res+output;
             }
             
             conn.disconnect();
             return res;
             
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "failed";
        } catch (IOException e){
            e.printStackTrace();
            return "failed";
        }
         
        
    } 
     public String consumirGet(String surl,String us, String textoGet){
         try {
            // URL url= new URL("http://172.31.89.249:8086/PruebaRest/rest/Servicios/Save");
             System.out.println(surl+"/"+us+"/"+textoGet);
             URL url= new URL(surl+"/"+us+"/"+textoGet);
             HttpURLConnection conn = (HttpURLConnection) url.openConnection();
             
             conn.setRequestMethod("GET");
             conn.setRequestProperty("Accept", "application/json");
             
             
             if(conn.getResponseCode() != HttpURLConnection.HTTP_OK){
                 throw  new RuntimeException("FAILED : HTTP error code: "+ conn.getResponseCode());
             }
             
             BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
             String output;
             String res="";
             System.out.println("OUTPUT from server ....");
             while((output = br.readLine())!=null){
                 System.out.println(output);
                 res=res+output;
             }
             
             conn.disconnect();
             return res;
             
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "failed";
        } catch (IOException e){
            e.printStackTrace();
            return "failed";
        }
     }     
    
}
