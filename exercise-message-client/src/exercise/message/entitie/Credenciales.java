package exercise.message.entitie;

import exercise.message.entitie.Seguridad;
import com.google.gson.Gson;


public class Credenciales {
	private String Usuario;
	private String Password;

	public String getUsuario() {
		return Usuario;
	}

	public void setUsuario(String usuario) {
		Usuario = usuario;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public Credenciales(String usuario, String password) {
		Usuario = usuario;
		Password = password;
	}

	public String Encriptar(String KeyString) throws Exception {

		Seguridad seguridad = new Seguridad(KeyString);

		String usuarioEncriptado = seguridad.Encriptar(Usuario);
		String passwordEncriptado = seguridad.Encriptar(Password);

		Credenciales cr = new Credenciales(usuarioEncriptado, passwordEncriptado);
		String jsonCredenciales = new Gson().toJson(cr);

		return seguridad.Encriptar(jsonCredenciales);
	}
}
