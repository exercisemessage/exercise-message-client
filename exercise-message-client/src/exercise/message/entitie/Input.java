package exercise.message.entitie;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class Input {
	private String credenciales;
	private String object;

	public String getCredenciales() {
		return credenciales;
	}

	public void setCredenciales(String credenciales) {
		this.credenciales = credenciales;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public Input(String input, String key) throws JsonSyntaxException, Exception {
		
		Input eInput = new Gson().fromJson(input, Input.class);

		this.setCredenciales(eInput.getCredenciales());
		this.setObject(eInput.getObject());
	}

	public Input() {
		
	}

}
