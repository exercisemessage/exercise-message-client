package com.exercise.message.entities.input;

public class ListarMensajeInput {
	String Nombre;
	String Credenciales;

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getCredenciales() {
		return Credenciales;
	}

	public void setCredenciales(String credenciales) {
		Credenciales = credenciales;
	}
	
}
