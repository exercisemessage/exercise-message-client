package exercise.message.entitie;

import java.io.Serializable;



/**
 * The persistent class for the login database table.
 * 
 */

public class Login {
	
	private Integer id;

	private Integer estado;

	private String password;

	private String usuario;

	public Login() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEstado() {
		return this.estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

}