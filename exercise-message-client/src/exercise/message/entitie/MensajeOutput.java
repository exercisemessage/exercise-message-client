package exercise.message.entitie;

import java.util.Date;

public class MensajeOutput {
	private String Mensaje;
	private String UsuarioFrom;
	private String UsuarioTo;
	private Date FechaEnvio;

	public String getMensaje() {
		return Mensaje;
	}

	public void setMensaje(String mensaje) {
		Mensaje = mensaje;
	}

	public String getUsuarioFrom() {
		return UsuarioFrom;
	}

	public void setUsuarioFrom(String usuarioFrom) {
		UsuarioFrom = usuarioFrom;
	}

	public String getUsuarioTo() {
		return UsuarioTo;
	}

	public void setUsuarioTo(String usuarioTo) {
		UsuarioTo = usuarioTo;
	}

	public Date getFechaEnvio() {
		return FechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		FechaEnvio = fechaEnvio;
	}

}
