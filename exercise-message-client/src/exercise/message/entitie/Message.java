package exercise.message.entitie;

import java.io.Serializable;

import java.util.Date;


/**
 * The persistent class for the message database table.
 * 
 */

public class Message {
	
	private Integer id;

	private Date fechaEnvio;


	private Date fechaVisto;

	private String mensaje;


	private Usuario usuario1;


	private Usuario usuario2;

	public Message() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFechaEnvio() {
		return this.fechaEnvio;
	}

	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	public Date getFechaVisto() {
		return this.fechaVisto;
	}

	public void setFechaVisto(Date fechaVisto) {
		this.fechaVisto = fechaVisto;
	}

	public String getMensaje() {
		return this.mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Usuario getUsuario1() {
		return this.usuario1;
	}

	public void setUsuario1(Usuario usuario1) {
		this.usuario1 = usuario1;
	}

	public Usuario getUsuario2() {
		return this.usuario2;
	}

	public void setUsuario2(Usuario usuario2) {
		this.usuario2 = usuario2;
	}

}