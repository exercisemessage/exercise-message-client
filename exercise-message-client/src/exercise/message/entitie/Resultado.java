package com.exercise.message.entities;

public class Resultado {

	private int Codigo;
	private String Mensaje;
	private String ObjetoJson;

	public int getCodigo() {
		return Codigo;
	}

	public void setCodigo(int codigo) {
		Codigo = codigo;
	}

	public String getMensaje() {
		return Mensaje;
	}

	public void setMensaje(String mensaje) {
		Mensaje = mensaje;
	}

	public String getObjetoJson() {
		return ObjetoJson;
	}

	public void setObjetoJson(String objetoJson) {
		ObjetoJson = objetoJson;
	}

	public Resultado(int codigo, String mensaje, String objetoJson) {
		Codigo = codigo;
		Mensaje = mensaje;
		ObjetoJson = objetoJson;
	}

	public Resultado() {
		Codigo = 0;
		Mensaje = "";
		ObjetoJson = null;
	}
	public Resultado(String resultado){
		
	}
}
