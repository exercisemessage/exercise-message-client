/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise.message.entitie;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import sun.misc.BASE64Encoder;

public class Seguridad {
	static final String HASH_ALGORITHM = "SHA";
	static final int SALT_LENGTH = 6;
	String KeyString;

	public Seguridad(String keyString) {
		this.KeyString = keyString;
	}

	public String Encriptar(String Text) throws Exception {
		byte[] salt = generateSalt(String.valueOf(this.KeyString).toLowerCase());
		String hash = calcultateHash(Text.getBytes(), salt);
		Text = null;
		return hash;
	}

	private static byte[] generateSalt(String seed) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA");

		md.update(seed.getBytes());
		byte[] digested = md.digest();
		md.reset();
		md = null;

		byte[] toReturn = new byte[6];
		System.arraycopy(digested, 0, toReturn, 0, 6);

		return toReturn;
	}

	private static String calcultateHash(byte[] clave, byte[] salt) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA");

		md.update(clave);
		md.update(salt);
		byte[] toReturn = md.digest();
		md.reset();
		md = null;

		Arrays.fill(clave, (byte) 0);
		clave = null;

		String base64 = new BASE64Encoder().encode(toReturn);
		return base64;
	}
}