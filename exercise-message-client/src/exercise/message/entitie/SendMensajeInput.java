package exercise.message.entitie;

public class SendMensajeInput {
	private String Mensaje;
	private String NombreFrom;
	private String NombreTo;

	public String getMensaje() {
		return Mensaje;
	}

	public void setMensaje(String mensaje) {
		Mensaje = mensaje;
	}

	public String getNombreFrom() {
		return NombreFrom;
	}

	public void setNombreFrom(String nombreFrom) {
		NombreFrom = nombreFrom;
	}

	public String getNombreTo() {
		return NombreTo;
	}

	public void setNombreTo(String nombreTo) {
		NombreTo = nombreTo;
	}

	public SendMensajeInput(String mensaje, String nombreFrom, String nombreTo) {
		Mensaje = mensaje;
		NombreFrom = nombreFrom;
		NombreTo = nombreTo;
	}

	public SendMensajeInput() {
	}
}
