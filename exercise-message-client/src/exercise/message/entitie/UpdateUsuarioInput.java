package exercise.message.entitie;

public class UpdateUsuarioInput {
	String OldNombre;
	String NewNombre;
	
	public String getOldNombre() {
		return OldNombre;
	}
	public void setOldNombre(String oldNombre) {
		OldNombre = oldNombre;
	}
	public String getNewNombre() {
		return NewNombre;
	}
	public void setNewNombre(String newNombre) {
		NewNombre = newNombre;
	}
	
	
}
